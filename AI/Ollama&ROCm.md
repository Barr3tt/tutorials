# Ollama w/ ROCm
## Uninstall the standard Ollama if you have it installed.
```
sudo rm /usr/local/bin/ollama
sudo systemctl stop ollama
sudo systemctl disable ollama
sudo rm /etc/systemd/system/ollama.service
sudo systemctl daemon-reload
sudo userdel ollama
sudo groupdel ollama
```
## Now run the ollama ROCm container.
Im using podman in this case but docker should be much easier and shouldnt require any extra permissions.

Here are the commands for a rootless podman container.
I am currently running Fedora 39 which uses SELinux. So I need to do some permission magic.
```
sudo setsebool -P container_use_devices=true
```
Now we just run the container! This command will depend on what GPU you are using. For AMD 7000 series, use the command provided below.
```
podman run -d --env "HSA_OVERRIDE_GFX_VERSION=11.0.0" --device=/dev/kfd:/dev/kfd --device=/dev/dri:/dev/dri -v ollama:/root/.ollama -p 11434:11434 --name ollama docker.io/ollama/ollama:0.1.28-rocm
```
For AMD 6000, and the Mobile 600 Series. Use the command below.
```
podman run -d --env "HSA_OVERRIDE_GFX_VERSION=10.3.0" --device=/dev/kfd:/dev/kfd --device=/dev/dri:/dev/dri -v ollama:/root/.ollama -p 11434:11434 --name ollama docker.io/ollama/ollama:0.1.28-rocm
```

For mobile GPUs the more VRAM allocated the better. 

You may want to modify the above command if you are using multimodal models that may need access to specific directories.
## Create an alias so running just "ollama" works.
I added this to my .bashrc
```
alias ollama='podman exec -it ollama ollama'
```
