Installing QEMU on SteamOS is possible but not permanent due to the limitations of how Steam's Immutable OS works. Any changes you make to the read only system will be reverted on every major SteamOS release.

Reguardless... Here is how you install Virt Manager and QEMU.
## Disable Read Only and initialize the arch repos
```sh
sudo steamos-readonly disable

sudo rm -R /etc/pacman.d/gnupg 
sudo pacman-key --init && sudo pacman-key --populate archlinux
```

## Install QEMU with pacman
```sh
sudo pacman -S qemu virt-manager virt-viewer dnsmasq vde2 bridge-utils openbsd-netcat swtpm ovmf

sudo systemctl enable libvirtd
sudo systemctl start libvirtd
```

## Change libvirt config
```sh
# Uncomment unix_sock_group, unix_sock_ro_perms, unix_sock_rw_perms
sudo nano /etc/libvirt/libvirtd.conf
sudo usermod -aG libvirt $(whoami)
sudo systemctl restart libvirtd
```
## Enable Read Only when done with changes
```bash
sudo steamos-readonly enable
```